(function () {
  angular
    .module('king.services.tourbuilder', [])
    .run(tourbuilderFunction);

  tourbuilderFunction.$inject = ['configService', '$window', '$translate'];

  function tourbuilderFunction(configService, $window,  $translate) {

    if (configService.services) {
      tourbuilderFunction(configService.services.tourbuilder.scope, configService.services.tourbuilder['scope-lang']);
    }
  
    function tourbuilderFunction(scopeData, scopeLang) {
      /* set language vars */
      var lang =  $translate.use() ? $translate.use().replace('_', '-') : "en-US";
      var scopeDataLang = scopeLang[lang];
      var code = scopeDataLang.codeLang != ""? scopeDataLang.codeLang : scopeData.code;
      var btntextlabel = scopeDataLang.btntextlabelLang != ""? scopeDataLang.btntextlabelLang : scopeData.btntextlabel;
      var btnlinkicon = scopeDataLang.btnlinkiconLang != ""? scopeDataLang.btnlinkiconLang : scopeData.btnlinkicon;
      /* END set language vars */

      var first =  getTour();
      console.log(first);

      if (first != true) {
        console.log("IN IF");
        return;
      } else {
        console.log("IN ELSE");
        load(scopeData);

        var endTour = Date.now() + (scopeData.tourtime * 86400000);
        $window.localStorage.setItem('tour', endTour);
        console.log(new Date(endTour));
      }

      function getTour(){
        tourDate = $window.localStorage.getItem('tour');
        var first = false;
        var now = Date.now();
        if (tourDate == null || tourDate <= now){
          first = true;
          $window.localStorage.removeItem('tour');
        }
        return first;
      }

      function load(scopeData) {
        document.getElementById('app').style.display = "none";

        var ifrm = document.createElement("iframe");
        ifrm.setAttribute('id', 'tour');
        ifrm.setAttribute("srcdoc", code);
        ifrm.style.width = "100vw";
        ifrm.style.height = "100vh";
        ifrm.style.border = '0px';

        var app = document.getElementById('app');
        app.parentNode.insertBefore(ifrm, app);

        if (scopeData.btnnotiniframe === true) {
          console.log("En boton");
          var btn = document.createElement("button");
          btn.setAttribute('id', 'btnclose');
          btn.style.position = "absolute";
          btn.style.margin = "20px";
          btn.style.zIndex = "9999";
          btn.style.padding = "0";
          btn.style.background = scopeData.btncolor;
          btn.style.border = '0px';
          if (scopeData.btnuselabel && btntextlabel) {
            btn.innerHTML = btntextlabel;
            btn.style.color = scopeData.iconcolor;
          } else {
            if(btnlinkicon && btnlinkicon != ""){
              console.log("custon button-icon");
              btn.innerHTML = '<img src=' + btnlinkicon + ' style="width:45px;height:45px;">';
            } else {
              console.log("default button-icon");
              btn.innerHTML = '<iron-icon icon="close" style="width:45px;height:45px; color:' + scopeData.iconcolor + '"></iron-icon>';
            }
          }
          if (scopeData.horizontalpos === 'right') {
            btn.style.right = "0";
          } else {
            btn.style.left = "0";
          }
          if (scopeData.verticalpos === 'top') {
            btn.style.top = "0";
          } else {
            btn.style.bottom = "0";
          }
          btn.onclick = function () {
            document.getElementById('tour').style.display = 'none';
            document.getElementById('btnclose').style.display = 'none';
            document.getElementById('app').style.display = 'block';
          }
          app.parentNode.insertBefore(btn, ifrm);
        } else {
          ifrm.srcdoc += '<script>function closeFrame(){parent.closefr();}</script>';

          window.closefr = function() {
            document.getElementById('tour').style.display = 'none';
            document.getElementById('app').style.display = 'block';
          }
        }
      }
    }
  }
})();